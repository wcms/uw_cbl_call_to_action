<?php

namespace Drupal\uw_cbl_call_to_action\EventSubscriber;

use Drupal\core_event_dispatcher\Event\Form\FormAlterEvent;
use Drupal\core_event_dispatcher\Event\Form\FormBaseAlterEvent;
use Drupal\core_event_dispatcher\Event\Form\FormIdAlterEvent;
use Drupal\preprocess_event_dispatcher\Event\BlockPreprocessEvent;
use Drupal\uw_cfg_common\CustomBlocks\UwCblBase;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Class UwCblCtaPreprocessBlock.
 */
class UwCblCtaPreprocessBlock extends UwCblBase implements EventSubscriberInterface {

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    return [
      BlockPreprocessEvent::name() => 'preprocessBlock',
    ];
  }

  /**
   * Preprocess blocks with CTAs and set variables.
   *
   * @param \Drupal\preprocess_event_dispatcher\Event\BlockPreprocessEvent $event
   *   The event.
   */
  public function preprocessBlock(BlockPreprocessEvent $event): void {

    // Check if we are on the right block to preprocess.
    if ($this->checkPreprocessBlock($event, 'uw_cbl_call_to_action')) {

      // Get the variables from the event.
      $variables = $event->getVariables();

      // Load in the block
      $block = $variables->getByReference('content');

      // If there are CTAs to process, process them.
      if (isset($block['field_uw_cta_details'])) {

        // Get the CTA blocks from the content variable.
        $cta_blocks = $block['field_uw_cta_details'];

        // Step through each CTA and get out the values.
        foreach ($cta_blocks as $key => $cta_block) {

          // If the key is an integer we have a CTA, so process it.
          if (is_int($key)) {

            // Need to reset the text details in case there are multiple CTAs.
            $text_details = [];

            // Set a variable with paragraph.
            $para = $cta_block['#paragraph'];

            // Get the text details, which is a paragraph.
            $cta_text_details = $para->get('field_uw_cta_text_details')->getValue();

            // Step through each of the text details and get the values.
            foreach ($cta_text_details as $cta_text_detail) {

              // Check if the text details has a target_id.
              if (isset($cta_text_detail['target_id']) && $cta_text_detail['target_id']) {

                // If there is an existing text details it will have a target_id,
                // and we need to load in the paragraph.
                $entity = \Drupal\paragraphs\Entity\Paragraph::load($cta_text_detail['target_id']);
              }
              else {

                // If it is a new text details we need to load in the entity.
                $entity = $cta_text_detail['entity'];
              }

              // If we are on an image text type, get variables.
              if ($entity->get('field_uw_cta_type_of_text')->getValue()[0]['value'] == 'image') {

                // Retrieve the file entity using the target_id from the image field.
                $file = \Drupal\file\Entity\File::load($entity->get('field_uw_cta_icon_image')->getValue()[0]['target_id']);

                // Retrieve the image entity using the file uri.
                $image = \Drupal::service('image.factory')->get($file->getFileUri());

                // Get the values of the text.
                $text_details[] = [
                  'text' => [
                    '#theme' => 'image_style',
                    '#width' => $image->getWidth(),
                    '#height' => $image->getHeight(),
                    '#uri' => $file -> getFileUri(),
                    '#style_name' => 'uw_is_icon',
                  ],
                  'type' => $entity->get('field_uw_cta_type_of_text')->getValue()[0]['value'],
                ];
              }
              // We have any type of text, get the varaibles.
              else {

                // Get the values of the text.
                $text_details[] = [
                  'text' => $entity->get('field_uw_cta_text')->getValue()[0]['value'],
                  'type' => $entity->get('field_uw_cta_type_of_text')->getValue()[0]['value'],
                ];
              }
            }

            // Get the values of the CTA into an array.
            $ctas[] = [

              // The link of the CTA.
              'link' => $para->get('field_uw_cta_link')->getValue() ? $para->get('field_uw_cta_link')->getValue()[0]['uri'] : NULL,

              // The text details of the CTA.
              'details' => $text_details ? $text_details : NULL,

              // The theme of the CTA.
              'theme' => $para->get('field_uw_cta_theme')->getValue()[0]['value'] ? $para->get('field_uw_cta_theme')->getValue()[0]['value'] : NULL,
            ];
          }
        }
      }

      // If there are CTAs, assign them to the variables for the template to use.
      if (isset($ctas)) {
        $variables->set('ctas', $ctas);
      }
    }
  }
}
