<?php

namespace Drupal\uw_cbl_call_to_action\EventSubscriber;

use Drupal\Core\Form\FormStateInterface;
use Drupal\core_event_dispatcher\Event\Form\FormAlterEvent;
use Drupal\core_event_dispatcher\Event\Form\FormBaseAlterEvent;
use Drupal\core_event_dispatcher\Event\Form\FormIdAlterEvent;
use Drupal\field_event_dispatcher\Event\Field\WidgetFormAlterEvent;
use Drupal\hook_event_dispatcher\HookEventDispatcherInterface;
use Drupal\uw_cfg_common\CustomBlocks\UwCblBase;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Class UwCblCtaAlterForm.
 */
class UwCblCtaAlterForm extends UwCblBase implements EventSubscriberInterface {

  /**
   * Alter form for CTA to add validation.
   *
   * @param \Drupal\core_event_dispatcher\Event\Form\FormAlterEvent $event
   *   The event.
   */
  public function alterForm(FormAlterEvent $event): void {

    // Check that we are on a CTA block.
    if ($this->checkLayoutBuilder($event, 'Call to action')) {

      // Get the form from the event.
      $form = &$event->getForm();

      // Add the validation for blockquote.
      $form['#validate'][] = [$this, 'uw_cbl_call_to_action_validation'];
    }
  }

  /**
   * Form validation for call to action.
   *
   * @param array $form
   *   The complete form structure.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  public function uw_cbl_call_to_action_validation(&$form, \Drupal\Core\Form\FormStateInterface $form_state) {

    // Get the settings from the form.
    $settings = $form_state->getValue('settings', NULL);

    // If there are settings, continue to process.
    if ($settings) {

      // If there is a block, continue to process.
      if ($block = $settings['block_form']) {

        // Variable to break out of loops.
        $error_is_set = FALSE;

        // Step through each of the ctas and check for text.
        foreach ($block['field_uw_cta_details'] as $cta_key => $cta_details) {

          // Ensure that we are on an actual CTA.
          if (is_int($cta_key)) {

            // Step through each of the text details for tHe CTA and check for text.
            foreach ($cta_details['subform']['field_uw_cta_text_details'] as $text_key => $text_details) {

              // Ensure that we are on a text.
              if (is_int($text_key)) {

                // If there is no value in the text, throw an error.
                if ($text_details['subform']['field_uw_cta_text'][0]['value'] == '') {

                  // Set the element to have the error.
                  $element = 'settings][block_form][field_uw_cta_details][' . $cta_key . '][subform][field_uw_cta_text_details][' . $text_key . '][subform][field_uw_cta_text][0][value';

                  // Set the form error.
                  $form_state->setErrorByName($element, 'You must enter text.');

                  $error_is_set = TRUE;
                }
              }
            }
          }

          // If there is an error already set, break out of the loop.
          if ($error_is_set) {
            break;
          }
        }
      }
    }
  }

  /**
   * Alter widget form for CTAs to added states to type of text.
   *
   * @param \Drupal\field_event_dispatcher\Event\Field\WidgetFormAlterEvent $event
   *   The event.
   */
  public function alterWidgetForm(WidgetFormAlterEvent $event): void {

    // Get the context from the event.
    $context = $event->getContext();

    /** @var \Drupal\field\Entity\FieldConfig $field_definition */
    $field_definition = $context['items']->getFieldDefinition();
    $paragraph_entity_reference_field_name = $field_definition->getName();

    if ($paragraph_entity_reference_field_name == 'field_uw_cta_text_details') {

      // Get the element from the event.
      $element = &$event->getElement();

      // Get the form state from the event.
      $form_state = &$event->getFormState();

      /** @see \Drupal\paragraphs\Plugin\Field\FieldWidget\ParagraphsWidget::formElement() */
      $widget_state = \Drupal\Core\Field\WidgetBase::getWidgetState($element['#field_parents'], $paragraph_entity_reference_field_name, $form_state);

      /** @var \Drupal\paragraphs\Entity\Paragraph $paragraph */
      $paragraph_instance = $widget_state['paragraphs'][$element['#delta']]['entity'];
      $paragraph_type = $paragraph_instance->bundle();

      // Determine which paragraph type is being embedded.
      if ($paragraph_type == 'uw_para_call_to_action_text') {

        // Counter to get the selector ids out.
        $count = 0;

        // Step through each of the parents to get out the selector ids.
        foreach ($element["subform"]["field_uw_cta_text"]["#parents"] as $parent) {

          // If the parents is an integer it is an id.  First integer is the
          // cta_id and the second is the cta_text_id.
          if (is_int($parent)) {
            if ($count == 0) {
              $cta_id = $parent;
            }
            else {
              $cta_text_id = $parent;
            }
            $count++;
          }
        }

        // Selector for dependee field.
        $selector = 'select[name="settings[block_form][field_uw_cta_details][' . $cta_id . '][subform][field_uw_cta_text_details][' . $cta_text_id . '][subform][field_uw_cta_type_of_text]';

        // Dependent fields.
        $element['subform']['field_uw_cta_text']['#states'] = [
          'visible' => [
            $selector => [
              ['value' => 'big'],
              ['value' => 'medium'],
              ['value' => 'small']
            ]
          ],
        ];

        // Dependent fields.
        $element['subform']['field_uw_cta_icon_image']['#states'] = [
          'visible' => [
            $selector => [
              ['value' => 'image']
            ]
          ],
        ];
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [
      HookEventDispatcherInterface::FORM_ALTER => 'alterForm',
      HookEventDispatcherInterface::WIDGET_FORM_ALTER => 'alterWidgetForm',
    ];
  }
}
